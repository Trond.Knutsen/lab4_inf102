package INF102.lab4.median;

import java.util.ArrayList;
import java.util.List;

public class QuickMedian implements IMedian {

    @Override
    public <T extends Comparable<T>> T median(List<T> list) {
        int medianIndex = list.size()/2; //Works on only odd numbered lists
        return findMedian(list, medianIndex);
    }

    private <T extends Comparable<T>> T findMedian(List<T> list, int medianIndex) {
        //Base case
        if (list.size() == 1) { //O(1)
            return list.get(0); 
        }

        List<T> listRight = new ArrayList<>();
        List<T> listLeft = new ArrayList<>();
        List<T> listPivot = new ArrayList<>();

        T pivot = list.get(0);

        for (int i = 0; i < list.size(); i++) { //O(n)
            T element = list.get(i);
            if (pivot.compareTo(element) == 0) {
                listPivot.add(element);
            } else if (pivot.compareTo(element) == 1) {
                listLeft.add(element);
            } else if (pivot.compareTo(element) == -1) {
                listRight.add(element);
            }
        }

        if (listLeft.size() > medianIndex) { 
            return findMedian(listLeft, medianIndex);
        } else if ((listLeft.size() + listPivot.size()) > medianIndex) {
            return listPivot.get(0);
        } else {
            medianIndex = medianIndex - (listLeft.size() + listPivot.size());
            return findMedian(listRight, medianIndex);
        }

    }

}
