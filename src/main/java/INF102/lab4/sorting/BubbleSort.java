package INF102.lab4.sorting;

import java.util.List;

public class BubbleSort implements ISort {

    @Override
    public <T extends Comparable<T>> void sort(List<T> list) { //O(n log n)
        
        if (list.size() == 0) { //0(1)
            throw new IndexOutOfBoundsException("List can not be empty");
        }
        
        for (int i = 0; i < list.size(); i++) { //0(n)
            boolean swap = true; //Tracks if there has been a swap
            
            for (int j = 0; j < list.size()-1-i; j++) { //0(log n)???
                T current = list.get(j); //0(1)
                T next = list.get(j+1); //0(1)

                if (current.compareTo(next) > 0) { //0(1)
                    list.set(j, next); //0(1)
                    list.set(j+1, current); //0(1)
                    swap = false; //0(1)
                }
            }
            if(swap){ //0(1)
                break;
            }
        }

    }
    
}
